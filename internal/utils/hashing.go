package utils

import (
	"crypto/sha256"
)

func Hashing(input string) ([]byte, error) {
	hasher := sha256.New()
	_, err := hasher.Write([]byte(input))
	if err != nil {
		return nil, err
	}
	hash := hasher.Sum(nil)
	return hash, nil
}
