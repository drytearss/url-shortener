package shorten

import (
	"encoding/hex"

	"gitlab.com/drytearss/url-shortener/internal/utils"
)

func Shorten(originalURL string) (string, error) {
	hash, err := utils.Hashing(originalURL)
	if err != nil {
		return "", err
	}
	shortURL := hex.EncodeToString(hash)[:8]
	return shortURL, nil
}
