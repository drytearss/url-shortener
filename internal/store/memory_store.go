package store

import "errors"

var (
	ErrNotFound = errors.New("not found")
	ErrKeyExist = errors.New("an entry with such a key exists")
)

type Store struct {
	db map[string]string
}

func NewStore() *Store {
	db := make(map[string]string)
	return &Store{
		db: db,
	}
}

func (s *Store) List() (map[string]string, error) {
	return s.db, nil
}

func (s *Store) Get(key string) (string, error) {
	value, ok := s.db[key]
	if !ok {
		return "", ErrNotFound
	}
	return value, nil
}

func (s *Store) Create(key, value string) (string, error) {
	_, ok := s.db[key]
	if ok {
		return "", ErrKeyExist
	}
	s.db[key] = value
	return key, nil
}
