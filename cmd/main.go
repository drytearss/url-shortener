package main

import (
	"log"
	"net/http"

	"gitlab.com/drytearss/url-shortener/internal/shorten"
	"gitlab.com/drytearss/url-shortener/internal/store"
)

func main() {
	store := store.NewStore()
	urlsHandler := NewUrlsHandler(store)

	mux := http.NewServeMux()
	mux.HandleFunc("GET /a", urlsHandler.ShortenHandler)
	mux.HandleFunc("GET /s/{short}", urlsHandler.RedirectHandler)

	server := &http.Server{
		Addr:    ":8080",
		Handler: mux,
	}
	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}

type urlsStore interface {
	List() (map[string]string, error)
	Get(key string) (string, error)
	Create(key, value string) (string, error)
}

func InternalServerErrorHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte("internal server error"))
}

func BadRequestErrorHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte("bad reequest"))
}

type urlsHandler struct {
	store urlsStore
}

func NewUrlsHandler(store urlsStore) *urlsHandler {
	return &urlsHandler{
		store: store,
	}
}

func (h *urlsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {}

func (h *urlsHandler) ShortenHandler(w http.ResponseWriter, r *http.Request) {
	originalURL := r.URL.Query().Get("url")
	if originalURL == "" {
		BadRequestErrorHandler(w, r)
		return
	}
	shortURL, err := shorten.Shorten(originalURL)
	if err != nil {
		InternalServerErrorHandler(w, r)
		return
	}
	shortURL, err = h.store.Create(shortURL, originalURL)
	if err != nil {
		InternalServerErrorHandler(w, r)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(shortURL))
}

func (h *urlsHandler) RedirectHandler(w http.ResponseWriter, r *http.Request) {
	shortULR := r.PathValue("short")
	originalURL, err := h.store.Get(shortULR)
	if err != nil {
		InternalServerErrorHandler(w, r)
		return
	}
	http.Redirect(w, r, originalURL, http.StatusFound)
}
